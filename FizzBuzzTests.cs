using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

[TestFixture]
public class FizzBuzzTests
{
    public FizzBuzz fizzBuzz;
    public string resultedString;

    [SetUp]
    public void SetUp()
    {
        GivenAFizzBuzzClass();
    }

    [Test]
    public void ANumberDivisibleBy3ReturnFizz()
    {
        WhenSampleNumber(6);

        ThenResultedStringIs("Fizz");
    }

    [Test]
    public void ANumberDivisibleBy5ReturnBuzz()
    {
        WhenSampleNumber(10);

        ThenResultedStringIs("Buzz");
    }

    [Test]
    public void ANumberDivisibleBy3And5ReturnFizzBuzz()
    {
        WhenSampleNumber(30);

        ThenResultedStringIs("FizzBuzz");
    }

    [Test]
    public void ANumberThatContains3ReturnFizz()
    {
        WhenSampleNumber(13);

        ThenResultedStringIs("Fizz");
    }

    [Test]
    public void ANumberThatContains5ReturnBuzz()
    {
        WhenSampleNumber(59);

        ThenResultedStringIs("Buzz");
    }

    [Test]
    public void ANumberThatContains3and5ReturnFizzBuzz()
    {
        WhenSampleNumber(532);

        ThenResultedStringIs("FizzBuzz");
    }

    public void GivenAFizzBuzzClass()
    {
        fizzBuzz = new FizzBuzz();
    }

    public void WhenSampleNumber(int number)
    {
        resultedString = fizzBuzz.SampleNumber(number);
    }

    public void ThenResultedStringIs(string expectedResult)
    {
        Assert.That(resultedString, Is.EqualTo(expectedResult));
    }
}
