using System;
using System.Collections;
using System.Collections.Generic;

public class FizzBuzz
{
    public FizzBuzz()
    {
    }

    public string SampleNumber(int number)
    {
        string resultedString = "";
        if (DivisibleBy3Or5(number) || Contains3Or5(number))
        {
            if (DivisibleBy3(number) || Contains3(number))
                resultedString += "Fizz";
            if (DivisibleBy5(number) || Contains5(number))
                resultedString += "Buzz";
        }
        else
        {
            resultedString += number;
        }
        return resultedString;
    }

    public bool Contains3Or5(int number)
    {
        if (Contains3(number) ||
            Contains5(number))
            return true;
        return false;
    }

    private bool Contains3(int number)
    {
        string stringNumber = number.ToString();
        return stringNumber.Contains("3");
    }

    private bool Contains5(int number)
    {
        string stringNumber = number.ToString();
        return stringNumber.Contains("5");
    }

    public bool DivisibleBy3Or5(int number)
    {
        if (DivisibleBy3(number) ||
            DivisibleBy5(number))
            return true;
        return false;
    }

    private static bool DivisibleBy5(int number)
    {
        return number % 5 == 0;
    }

    private static bool DivisibleBy3(int number)
    {
        return number % 3 == 0;
    }

    public void PrintTheFirst(int numbers)
    {
        for (int i = 1; i < numbers; i++)
        {
            Console.Write(SampleNumber(i));
        }
    }
}
